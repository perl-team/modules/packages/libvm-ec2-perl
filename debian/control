Source: libvm-ec2-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libanyevent-cachedns-perl <!nocheck>,
                     libanyevent-http-perl <!nocheck>,
                     libanyevent-perl <!nocheck>,
                     libjson-perl <!nocheck>,
                     libstring-approx-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     libwww-perl <!nocheck>,
                     libxml-simple-perl <!nocheck>,
                     rename
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libvm-ec2-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libvm-ec2-perl.git
Homepage: https://metacpan.org/release/VM-EC2
Rules-Requires-Root: no

Package: libvm-ec2-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libanyevent-cachedns-perl,
         libanyevent-http-perl,
         libanyevent-perl,
         libjson-perl,
         libstring-approx-perl,
         liburi-perl,
         libwww-perl,
         libxml-simple-perl
Description: module providing controls on Amazon EC2 and Eucalyptus
 This is an interface to the 2014-05-01 version of the Amazon AWS API
 (http://aws.amazon.com/ec2). It was written provide access to the new tag and
 metadata interface that is not currently supported by Net::Amazon::EC2, as
 well as to provide developers with an extension mechanism for the API. This
 library will also support the Eucalyptus open source cloud
 (http://open.eucalyptus.com).
 .
 The main interface is the VM::EC2 object, which provides methods for
 interrogating the Amazon EC2, launching instances, and managing instance
 lifecycle.
